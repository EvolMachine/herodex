var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var ionic_1 = require('ionic-framework/ionic');
var marvelApi_1 = require('../../services/marvelApi');
var HeroSearch = (function () {
    function HeroSearch(marvelApi, nav) {
        this.marvelApi = marvelApi;
        this.nav = nav;
        this.noneFound = false;
    }
    HeroSearch.prototype.searchHeroes = function (heroName) {
        var _this = this;
        this.marvelApi.searchForHero(heroName)
            .subscribe(function (response) {
            _this.foundHeroes = response.data.results;
            _this.noneFound = _this.foundHeroes.length == 0;
            _this.attributionLink = marvelApi_1.MarvelApi.getAttributionLink(response);
        }, function (error) { return _this.errorMessage = error; });
    };
    HeroSearch.prototype.showHeroDetailModal = function (hero) {
        var heroDetailModal = ionic_1.Modal.create(HeroDetail, { hero: hero });
        this.nav.present(heroDetailModal);
    };
    HeroSearch = __decorate([
        ionic_1.Page({
            templateUrl: 'build/pages/heroSearch/heroSearch.html',
            providers: [
                marvelApi_1.MarvelApi
            ]
        }), 
        __metadata('design:paramtypes', [marvelApi_1.MarvelApi, ionic_1.NavController])
    ], HeroSearch);
    return HeroSearch;
})();
exports.HeroSearch = HeroSearch;
var HeroDetail = (function () {
    function HeroDetail(params, viewCtrl) {
        this.hero = params.get("hero");
        console.log(this.hero);
        this.viewCtrl = viewCtrl;
    }
    HeroDetail.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    HeroDetail = __decorate([
        ionic_1.Page({
            templateUrl: 'build/pages/heroSearch/heroDetail.html'
        }), 
        __metadata('design:paramtypes', [ionic_1.NavParams, ionic_1.ViewController])
    ], HeroDetail);
    return HeroDetail;
})();
exports.HeroDetail = HeroDetail;
