import {Page, Modal, NavController, NavParams, ViewController} from 'ionic-framework/ionic';

import {MarvelApi} from '../../services/marvelApi';
import {Hero} from '../../models/hero';


@Page({
    templateUrl: 'build/pages/heroSearch/heroSearch.html',
    providers: [
        MarvelApi
    ]
})
export class HeroSearch {
    errorMessage: string;
    foundHeroes: Hero[];
    attributionLink: string;
    noneFound: boolean;

    constructor(private marvelApi: MarvelApi, private nav: NavController) {
        this.noneFound = false;
    }

    searchHeroes(heroName) {
        this.marvelApi.searchForHero(heroName)
            .subscribe(
                response => {
                    this.foundHeroes = <Hero[]> response.data.results;
                    this.noneFound = this.foundHeroes.length == 0;
                    this.attributionLink = MarvelApi.getAttributionLink(response)
                },
                error => this.errorMessage = <any>error);
    }

    showHeroDetailModal(hero: Hero) {
        let heroDetailModal = Modal.create(HeroDetail, {hero: hero});
        this.nav.present(heroDetailModal);
    }
}

@Page({
    templateUrl: 'build/pages/heroSearch/heroDetail.html'
})
export class HeroDetail {
    hero: Hero;
    viewCtrl: ViewController;

    constructor(params: NavParams, viewCtrl: ViewController) {
        this.hero = params.get("hero");
        console.log(this.hero);
        this.viewCtrl = viewCtrl;
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
}
