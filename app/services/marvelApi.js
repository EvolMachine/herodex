var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('angular2/core');
var http_1 = require('angular2/http');
var Observable_1 = require('rxjs/Observable');
require('rxjs');
var MarvelApi = (function () {
    function MarvelApi(http) {
        this.http = http;
        this.API_KEY = '3cab615d04e77dafbfdaa753e81c8995';
        this.BASE_API_URL = 'http://gateway.marvel.com/v1/public/';
    }
    MarvelApi.prototype.searchForHero = function (heroName) {
        var requestUrl = this.BASE_API_URL + 'characters?nameStartsWith=' + heroName + '&apikey=' + this.API_KEY;
        return this.http.get(requestUrl)
            .map(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    MarvelApi.prototype.handleError = function (error) {
        console.error(error);
        return Observable_1.Observable.throw(error.json().error || 'Server error');
    };
    MarvelApi.getAttributionLink = function (response) {
        var attributionParts = response.attributionHTML.split(">");
        attributionParts[0] += "target='_blank'";
        return attributionParts.join(">");
    };
    MarvelApi = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], MarvelApi);
    return MarvelApi;
})();
exports.MarvelApi = MarvelApi;
