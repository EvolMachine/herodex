import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs';

import {Hero} from '../models/hero';

@Injectable()
export class MarvelApi {
    API_KEY = '3cab615d04e77dafbfdaa753e81c8995';
    BASE_API_URL = 'http://gateway.marvel.com/v1/public/';

    constructor(private http: Http) {}

    searchForHero(heroName) {
        let requestUrl = this.BASE_API_URL + 'characters?nameStartsWith=' + heroName + '&apikey=' + this.API_KEY;

        return this.http.get(requestUrl)
            .map(res => <any> res.json())
            .catch(this.handleError);
    }

    private handleError(error) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }

     static getAttributionLink(response) {
        let attributionParts = response.attributionHTML.split(">");
        attributionParts[0] += `target='_blank'`;
        return attributionParts.join(">");
    }
}